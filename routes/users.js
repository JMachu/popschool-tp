var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* GET popschool */
router.get('/popschool', function(req, res, next) {
    res.render('popschool', { user: 'Popschool' });
});

module.exports = router;
